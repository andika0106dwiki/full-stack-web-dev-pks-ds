<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        return response()->json(Blog::all(), 200);
    }

    public function create(Request $request)
    {
        Blog::create($request->all());

        return response()->json([
            'success' => true,
            'message' => 'Data berhasil dibuat',
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $check_blog = Blog::firstWhere('id', $id);
        if ($check_blog) {
            $blog = Blog::find($id);
            $blog->title = $request->title;
            $blog->description = $request->description;
            $blog->save();

            return response()->json([
                'success' => true,
                'message' => 'Data berhasil diupdate',
                'update' => $blog,
            ], 200);
        } else {
            return response()->json([
                'success' => 'Not Found',
                'message' => 'Data tidak ada !',
            ], 404);
        }
    }

    public function delete($id)
    {
        $check_blog = Blog::firstWhere('id', $id);
        if ($check_blog) {
            Blog::destroy($id);
            return response([
                'status' => 'Ok',
                'message' => 'Data berhasil dihapus',
            ], 200);
        } else {
            return response([
                'status' => 'Not Found',
                'message' => 'Data tidak ditemukan !',
            ], 404);
        }
    }
}

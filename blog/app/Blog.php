<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';
    // public $timestamps = false;
    protected $fillable = ['title', 'description'];
}

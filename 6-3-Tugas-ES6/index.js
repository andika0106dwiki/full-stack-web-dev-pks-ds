//Soal 1
const hitung = () => {
    let lebar = 5
        let panjang = 10
        let luas = panjang * lebar;
        let keliling = 2 * (panjang + lebar);
    
        const obj = {luas, keliling}

        console.log(obj);
}
hitung()

//Soal 2
const newFunction = (firstname, lastname) => {
    return {
        firstname,
        lastname,
        fullname: function(){
            console.log(firstname + " " + lastname)
        }
    }
}

newFunction("William", "Imoh").fullname();

//Soal 3
const newObject = {
    firstname   : "Muhammad",
    lastname    : "Iqbal Mubarok",
    address     : "Jalan Ranamanyar",
    hobby       : "playing football",
}
const { firstname, lastname, address, hobby, } = newObject
console.log(firstname, lastname, address, hobby)

//Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]
console.log(combined)

//Soal 5
const planet = "earth" 
const view = "glass"

const before = `Loren ${view} dolor sit amet consectetur adipiscing elit ${planet}`
console.log(before)
